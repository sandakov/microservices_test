import secrets
from datetime import datetime, timedelta

from sanic.response import json
from user_microservice.db import insert_one, find_one, update_one


def api_auth(func):
    async def auth_wrapper(request, *args, **kwargs):
        authenticated = False
        if 'token' in request.headers:
            token = await find_one('tokens', {'token': request.headers['token']})
            if token:
                token_valid_date = token['valid']
                if datetime.now().timestamp() > float(token_valid_date):
                    token_content = {
                        '_id': token['_id'],
                        'valid': (datetime.now() + timedelta(days=2)).timestamp(),
                        'token': secrets.token_urlsafe(32)
                    }

                    result = await update_one(
                        'tokens',
                        {'_id': token_content['_id']},
                        {
                            '$set': {
                                'valid': token_content['valid'],
                                'token': token_content['token']
                            }
                        }
                    )
                    return json({'token': token_content['token']}, 202)
                else:
                    authenticated = True

        if 'username' and 'password' in request.headers and not authenticated:
            user = await find_one('users', {'username': request.headers['username'], 'password': request.headers['password']})
            if user:
                token = await find_one('tokens', {'user_id': user['_id']})
                if token:
                    return json({'token': token['token']}, 202)

                token_context = {
                    'user_id': user['_id'],
                    'token': secrets.token_urlsafe(32),
                    'valid': (datetime.now() + timedelta(days=2)).timestamp()
                }
                token = await insert_one('tokens', token_context)
                return json({'token': str(token.inserted_id)}, 201)

        if not authenticated:
            return json('Unauthorized', 401)

        return await func(request, *args, **kwargs)

    return auth_wrapper
