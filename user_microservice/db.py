from motor.motor_asyncio import AsyncIOMotorClient
import asyncio


def db_init(sanic, loop):
    global db, db_users
    client = AsyncIOMotorClient('mongodb://localhost:27017/', io_loop=loop)
    db = client.get_database('np_microservices')
    db_users = db['users']
    db_tokens = db['tokens']
    db_offers = db['offers']


async def find_one(collection: str, body):
    collection = getattr(db, collection)
    doc = await collection.find_one(body)
    if doc:
        return doc
    return None


async def insert_one(collection: str, body):
    collection = getattr(db, collection)
    doc = await collection.insert_one(body)
    if doc:
        return str(doc.inserted_id)
    return None


async def update_one(collection: str, q_filter, body):
    collection = getattr(db, collection)
    doc = await collection.update_one(q_filter, body)
    if doc:
        return doc
    return None
