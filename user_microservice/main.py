import asyncio

import uvloop
from bson import ObjectId
from sanic import Sanic
from sanic.exceptions import InvalidUsage
from sanic.response import json as sanic_json
from cerberus.validator import Validator

from user_microservice.db import db_init, find_one, insert_one
from user_microservice.decorators import api_auth
import json

app = Sanic(__name__)

loop = asyncio.get_event_loop()
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

user_schema = {
        'username': {'type': 'string'},
        'password': {'type': 'string'},
        'created_at': {'type': 'integer'}
    }


@app.route('/user/registry', methods=['POST'])
@api_auth
async def register(request):
    try:
        json_body = request.json
    except InvalidUsage:
        return sanic_json('Error parsing json', 400)

    v = Validator(user_schema)
    if not v.validate(json_body):
        return sanic_json('Error schema json', 400)

    user_c = await find_one('users', {'username': json_body['username']})
    if user_c:
        return sanic_json('User already exist', 400)

    user = await insert_one('users', json_body)
    return sanic_json(user, 201)


@app.route('/user/<user_id:string>', methods=['GET'])
@api_auth
async def user_info(request, user_id: str):
    user = await find_one('users', {'_id': ObjectId(user_id)})
    if user:
        user['_id'] = str(user['_id'])
        return sanic_json(json.dumps(user, sort_keys=True, indent=4), 200)
    return sanic_json('User not found', 404)


@app.route('/user/auth',  methods=['POST'])
@api_auth
async def user_auth(request):
    token = await find_one('tokens', {'token': request.headers['token']})
    if token:
        return sanic_json(str(token['user_id']), 200)
    return sanic_json('User not found', 404)


@app.listener('before_server_start')
def init(sanic, loop):
    db_init(sanic, loop)


app.run(host="0.0.0.0", port=8000, workers=3, debug=True, loop=loop)
