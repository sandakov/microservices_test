from views import register, auth, user_info, create_offer, offer_info

urlpatterns = [
    (r'^user/registry', register),
    (r'^user/auth', auth),
    (r'^user/(\d+)', user_info),
    (r'^offer/create', create_offer),
    (r'^offer', offer_info),

]
