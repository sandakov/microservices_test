import secrets
from http.server import SimpleHTTPRequestHandler
import json


def generate_token():
    return secrets.token_urlsafe(32)


def send_response(request: SimpleHTTPRequestHandler, code: int=200, headers: dict=None, body=None):
    if not request:
        return False

    if headers:
        for header in headers:
            request.send_header(f'{header}', f'{headers[header]}')
        request.end_headers()
    request.send_response(code)
    body = {'response': body}
    body = json.dumps(body, indent=4, sort_keys=True)
    body = '\n%s\n' % body
    cont = body.encode('utf-8')
    request.wfile.write(cont)
    return True




