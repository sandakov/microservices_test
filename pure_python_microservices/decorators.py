from db import db_auth_credentials, db_auth_token
from utils import send_response


def api_method(method):
    def decorator(func):
        def wrapper(request, *args):
            if request.command not in method:
                return send_response(request, code=405, body='Method Not Allowed')
            return func(request, *args)
        return wrapper
    return decorator


def api_auth(func):
    def auth_wrapper(request, *args):
        token = request.headers.get('token')
        username = request.headers.get('username')
        password = request.headers.get('password')

        if token:
            result = db_auth_token(token)
            if not result:
                return send_response(request, code=401, body='Unauthenticated')

        elif username and password:
            db_token = db_auth_credentials(username, password)
            if db_token:
                return send_response(request, code=200, body=db_token)

            return send_response(request, code=401, body='Unauthenticated')
        else:
            return send_response(request, code=401, body='Unauthenticated')

        return func(request, *args)

    return auth_wrapper

