import sqlite3
import os
from datetime import datetime, timedelta
import settings

from utils import generate_token

db = sqlite3.connect('./db.sqlite')
db.execute('pragma foreign_keys = on')
TOKEN_TTL = getattr(settings, 'TOKEN_TTL', 3600 * 24 * 2)


def db_init():

    db.execute("""
    CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        username nvarchar(50) NOT NULL UNIQUE, 
        password nvarchar(50) NOT NULL,
        created_at integer
    )
    """)

    db.execute("""
        CREATE TABLE IF NOT EXISTS offers (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            user integer REFERENCES users(id) ON DELETE CASCADE, 
            title text,
            text text,
            created_at integer
        )
    """)

    db.execute("""
        CREATE TABLE IF NOT EXISTS tokens (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            user integer REFERENCES users(id) ON DELETE CASCADE, 
            token text,
            expired integer
        )
    """)


def db_insert(table: str, cols: tuple=None, values: tuple=None):
    if not cols or not values:
        return

    values_count = '?,'*len(values)
    cols_str = ', '.join(cols)
    query = f'INSERT INTO {table}({cols_str}) VALUES ({values_count[:-1]})'
    try:
        row = db.execute(query, values)
        db.commit()
        return row.lastrowid
    except sqlite3.IntegrityError:
        return False  # Already exist or an error occurred


def db_read(table: str, cols: tuple=None, where: str=None):
    if not cols:
        cols = '*'
    else:
        cols = ', '.join(cols)

    if where:
        where = f'WHERE {where}'

    query = f'SELECT {cols} FROM {table} {where}'
    data = db.execute(query).fetchall()
    return data


def db_auth_credentials(username: str, password: str):
    query = 'SELECT id from users where username=? and password=?'
    cursor = db.execute(query, (username, password))
    data = cursor.fetchall()
    for row in data:
        if row[0] > 0:
            db_data = db_read('tokens', ('token', 'expired'), f"user={row[0]}")
            if db_data:
                token = db_data[0][0]
                expired = db_data[0][1]
                if datetime.now().timestamp() > float(expired):
                    token = generate_token()
            else:
                token = generate_token()
                date = (datetime.now() + timedelta(seconds=TOKEN_TTL)).timestamp()
                db_insert(
                    'tokens',
                    cols=('user', 'token', 'expired'),
                    values=(row[0], token, date)
                )
            return token
        else:
            return False


def db_auth_token(token: str):
    query = 'SELECT token, expired FROM tokens WHERE token=?'
    created = False
    cursor = db.execute(query, (token, ))
    data = cursor.fetchone()
    if data:
        db_token, date = data
        if datetime.now().timestamp() > date:
            delete_token_query = 'DELETE FROM tokens WHERE token=?'
            db.execute(delete_token_query, (token,))
            token = generate_token()
            created = True
    else:
        return False

    return token, created
