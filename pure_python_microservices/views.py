import re

from db import db_insert, db_read
from decorators import api_method, api_auth
import json
from json.decoder import JSONDecodeError
from utils import send_response


@api_method(['POST'])
@api_auth
def register(request):
    content_len = int(request.headers.get('content-length', 0))
    body = request.rfile.read(content_len).decode('utf-8')

    try:
        json_obj = json.loads(body)
    except JSONDecodeError:
        return send_response(request, code=400, body='error decode json')

    required_fields = {'username', 'password', 'created_at'}
    json_fields = set(list(json_obj.keys()))

    integrity_respected = required_fields.issubset(json_fields)
    if integrity_respected:
        try:
            float(json_obj['created_at'])
        except:
            return send_response(request, code=400, body='Integrity of parameters is not respected')

        user_id = db_read('users', ('id', ), f"username=\"{json_obj['username']}\"")
        if user_id:
            return send_response(request, code=400, body='User is already exist')
        else:
            db_fields = tuple(json_fields)
            db_values = tuple(json_obj[field] for field in json_fields if field in required_fields)
            user_id = db_insert('users', db_fields, db_values)
            return send_response(request, code=201, body=f"{user_id}")

    else:
        return send_response(request, code=400, body='Integrity of parameters is not respected')


@api_method(['POST'])
def auth(request):
    content_len = int(request.headers.get('content-length', 0))
    post_body = request.rfile.read(content_len).decode('utf-8')

    try:
        json_obj = json.loads(post_body)
    except JSONDecodeError:
        return send_response(request, code=400, body='error decode json')
    required_fields = {'username', 'password'}
    json_fields = set(list(json_obj.keys()))
    integrity_respected = required_fields.issubset(json_fields)
    if integrity_respected:
        user_id = db_read(
            'users',
            ('id',),
            f"username=\"{json_obj['username']}\" and password=\"{json_obj['password']}\""
        )
        if user_id:
            return send_response(request, code=200, body=f"{user_id[0]}")
        else:
            return send_response(request, code=401, body='Incorrect credentials')

    else:
        return send_response(request, code=400, body='integrity of parameters is not respected')


@api_method(['GET'])
def user_info(request, user_id):
    cols = ('id', 'username', 'created_at')
    db_data = db_read('users', cols=cols, where=f"id={user_id}")
    if db_data:
        data_dict = {}
        for i in range(len(cols)):
            data_dict[cols[i]] = db_data[0][i]
        return send_response(request, code=200, body=data_dict)
    return send_response(request, code=404, body='User not found')


@api_method(['POST'])
@api_auth
def create_offer(request):
    content_len = int(request.headers.get('content-length', 0))
    body = request.rfile.read(content_len).decode('utf-8')

    try:
        json_obj = json.loads(body)
    except JSONDecodeError:
        return send_response(request, code=400, body='Error decode json')

    required_fields = {'user', 'title', 'text', 'created_at'}
    json_fields = set(list(json_obj.keys()))
    integrity_respected = required_fields.issubset(json_fields)
    if integrity_respected:
        try:
            float(json_obj['created_at'])
        except:
            return send_response(request, code=400, body='Integrity of parameters is not respected')

        db_fields = tuple(json_fields)
        db_values = tuple(json_obj[field] for field in json_fields if field in required_fields)
        offer_id = db_insert('offers', db_fields, db_values)
        if offer_id:
            return send_response(request, code=201, body=f"{offer_id}")

    # else:
    return send_response(request, code=400, body='Integrity of parameters is not respected')


@api_method(['POST'])
@api_auth
def offer_info(request):
    content_len = int(request.headers.get('content-length', 0))
    body = request.rfile.read(content_len).decode('utf-8')

    try:
        json_obj = json.loads(body)
    except JSONDecodeError:
        return send_response(request, code=400, body='Error decode json')

    required_fields = {'offer_id', 'user_id'}
    json_fields = set(list(json_obj.keys()))
    fields = json_fields.intersection(required_fields)
    if fields:
        field_name = list(fields)[0]
        try:
            value = int(json_obj[field_name])
        except:
            return send_response(request, code=400, body='Integrity of parameters is not respected')

        field_name = re.sub(r'offer_id', 'id', field_name)
        field_name = re.sub(r'user_id', 'user', field_name)

    cols = ('id', 'user', 'title', 'text', 'created_at')
    db_data = db_read('offers', cols, f"{field_name}={value}")
    if db_data:
        data_dict = {}
        for row in db_data:
            row_data_dict = {}
            for i in range(len(cols)):
                row_data_dict[cols[i]] = row[i]
            data_dict[f"offer_{row[0]}"] = row_data_dict
        return send_response(request, code=200, body=data_dict)
    return send_response(request, code=404, body='Offer not found')


