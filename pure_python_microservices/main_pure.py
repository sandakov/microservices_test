import ssl
from datetime import datetime
from http.server import BaseHTTPRequestHandler, HTTPServer, SimpleHTTPRequestHandler
import re
from sys import argv

from db import db_init, db_insert, db_read


class MyHTTPRequestHandler(SimpleHTTPRequestHandler):

    def do_GET(self):
        self.url_parse()

    def do_POST(self):
        self.url_parse()

    def url_parse(self):
        from urls import urlpatterns
        for pattern in urlpatterns:
            path = self.path[1:] if self.path[0] == '/' else self.path
            reg = re.match(pattern[0], path)
            if reg:
                func = pattern[1]
                if reg.groups():
                    args = [group for group in reg.groups()]
                    func(self, *args)
                else:
                    func(self)
                break


def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    db_init()
    server_address = ('localhost', 8888)
    httpd = server_class(server_address, handler_class)
    httpd.socket = ssl.wrap_socket(httpd.socket, certfile='./server.pem', server_side=True)
    print(f'Started httpd on {server_address}...')
    httpd.serve_forever()


if __name__ == '__main__':
    arguments = argv[1:]
    if 'db_test_data' in arguments:
        username = 'test'
        password = 'test'
        user = db_read('users', ('id', ), f"username='{username}'")
        if user:
            if user[0][0] > 0:
                print(f"User '{username}' is already exist. Default password: '{password}''")
        else:
            idx = db_insert('users', ('username', 'password', 'created_at'), (username, password, datetime.now().timestamp()))
            if idx:
                print(f"Test user: '{username}' with password '{password}' added to db")

    run(handler_class=MyHTTPRequestHandler)






