import secrets
import json

from sanic.response import json as sanic_json
from offer_microservice.utils import make_request
from offer_microservice.settings import auth_url


def api_auth(func):
    async def auth_wrapper(request, *args, **kwargs):
        authenticated = False
        if 'token' in request.headers:
            response = await make_request('post', auth_url, headers={'token': request.headers['token']})
            if response.status_code == 200:
                authenticated = True

        if 'username' and 'password' in request.headers and not authenticated:
            headers = {'username': request.headers['username'], 'password': request.headers['password']}
            response = await make_request('post', auth_url, headers=headers)

            json_response = {}
            try:
                json_response = json.loads(response.content)
            except:
                pass

            if 'token' in json_response:
                return sanic_json({'token': json_response['token']}, 200)

        if not authenticated:
            return sanic_json('Unauthorized', 401)

        return await func(request, *args, **kwargs)

    return auth_wrapper
