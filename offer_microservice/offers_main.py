import asyncio
import json

import uvloop
from bson import ObjectId
from sanic import Sanic
from sanic.exceptions import InvalidUsage
from sanic.response import json as sanic_json
from cerberus.validator import Validator

from offer_microservice.db import db_init, find, insert_one, find_one
from offer_microservice.decorators import api_auth
from offer_microservice.utils import JSONEncoder

app = Sanic(__name__)

loop = asyncio.get_event_loop()
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

offer_schema = {
        'user_id': {'type': 'string'},
        'title': {'type': 'string'},
        'text': {'type': 'string'},
        'created_at': {'type': 'integer'}
    }


@app.route('/offer/create', methods=['POST'])
@api_auth
async def create_offer(request):
    try:
        json_body = request.json
    except InvalidUsage:
        return sanic_json('Error parsing json', 400)

    v = Validator(offer_schema)
    if not v.validate(json_body):
        return sanic_json('Error schema json', 400)

    offer = await insert_one('offers', json_body)
    return sanic_json(offer, 201)


@app.route('/offer', methods=['POST'])
@api_auth
async def get_offer(request):
    try:
        json_body = request.json
    except InvalidUsage:
        return sanic_json('Error parsing json', 400)

    json_data = {}
    if 'offer_id' in json_body:
        condition = {'_id': ObjectId(json_body['offer_id'])}
        offer = await find_one('offers', condition)
        if offer:
            json_data[str(offer['_id'])] = offer

    if 'user_id' in json_body:
        condition = {'user_id': json_body['user_id']}
        cursor = find('offers', condition)
        for row in await cursor.to_list(20):
            json_data[str(row['_id'])] = row

    return sanic_json(json_data, 200, sort_keys=True, indent=4, dumps=json.dumps, cls=JSONEncoder)


@app.listener('before_server_start')
def init(sanic, loop):
    db_init(sanic, loop)


app.run(host="0.0.0.0", port=8001, workers=3, debug=True, loop=loop)
