import requests
import json
from bson import ObjectId
from sanic.response import json as sanic_json


async def make_request(method, url, headers, json_data=None):
    request_method = getattr(requests, method, 'post')
    response = request_method(url, json=json_data, headers=headers)
    if response.content:
        return response
    return None


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)
